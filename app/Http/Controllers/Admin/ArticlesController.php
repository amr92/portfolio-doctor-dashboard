<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles=Article::all();
        return view('dashboard.views.articles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'text' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $article = new Article();
        $article->title = $request->title;
        $article->text = $request->text;
        if($request->hasFile('image')){
            $uploadedImage = $request->file('image');
            $imageName = time().'.'.$uploadedImage->getClientOriginalExtension();
            $uploadedImage->move(public_path('uploads/articles/'), $imageName);
            $article->image = 'uploads/articles/'.$imageName;
        }
        $article->save();
        flash(__('Article successfully added'))->success();
        return redirect()->route('admin.articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view('dashboard.views.articles.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'text' => 'required',
        ]);
        $article = Article::find($id);
        $article->title = $request->title;
        $article->text = $request->text;
        if($request->hasFile('image')){
            $uploadedImage = $request->file('image');
            $imageName = time().'.'.$uploadedImage->getClientOriginalExtension();
            $uploadedImage->move(public_path('uploads/articles/image/'), $imageName);
            $article->image = 'uploads/articles/image/'.$imageName;
        }
        $article->save();
        flash(__('Article successfully updated'))->success();
        return redirect()->route('admin.articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $article->delete();
        flash(__('Article has been deleted successfully'))->success();
        return redirect()->route('admin.articles');
    }
}
