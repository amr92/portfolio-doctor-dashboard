<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-profile"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><img src="{{asset('assets/dashboard/images/users/profile.png')}}" alt="user" /><span class="hide-menu">{{Auth::User()->name}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.profile')}}">My Profile </a></li>
                        <li><a href="{{route('logout')}}">Logout</a></li>
                    </ul>
                </li>
                <li class="nav-devider"></li>
                <li> <a href="{{route('dashboard')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
            </li>
                <li> <a href="{{route('admin.articles')}}" aria-expanded="false"><i class="mdi mdi-buffer"></i><span class="hide-menu">Articles</span></a>

                </li>
                <li> <a href="{{route('admin.contacts')}}" aria-expanded="false"><i class="mdi mdi-email"></i><span class="hide-menu">Contacts</span></a>
                </li>
                <li> <a href="{{route('admin.settings')}}" aria-expanded="false"><i class="mdi mdi-wrench"></i><span class="hide-menu">Settings</span></a>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>