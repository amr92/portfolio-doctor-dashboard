@extends('dashboard.layouts.app')
@section('content')


    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Create Article</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">Articles</li>
                    <li class="breadcrumb-item active">Create Article</li>
                </ol>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Create Articles</h4>
                        <form class="m-t-40" novalidate  method="POST" action="{{ route('admin.articles.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <h5>Title<span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="text" name="title" id="title" class="form-control" required data-validation-required-message="This field is required"> </div>
                            </div>
                            <div class="form-group">
                                <h5>Textarea <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <textarea name="text" id="text" class="form-control" required placeholder="Textarea text"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>File Input Field <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <input type="file" name="image" class="form-control" required> </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('admin.articles')}}" type="reset" class="btn btn-inverse">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
@endsection