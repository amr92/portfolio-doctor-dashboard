@extends('dashboard.layouts.app')
@section('content')

        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-themecolor">Email details</h3>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Email details</li>
                    </ol>
                </div>

            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="row">

                            <div class="col-xlg-10 col-lg-9 col-md-8 bg-light-part b-l">

                                <div class="card-body p-t-0">
                                    <div class="card b-all shadow-none">
                                        <div class="card-body">
                                            <h3 class="card-title m-b-0">Your message title goes here</h3>
                                        </div>
                                        <div>
                                            <hr class="m-t-0">
                                        </div>
                                        <div class="card-body">
                                            <div class="d-flex m-b-40">

                                                <div class="p-l-10">
                                                    <h4 class="m-b-0">{{$contact->name}}</h4>
                                                    <small class="text-muted">From: {{$contact->email}}</small>
                                                </div>
                                            </div>
                                            <p>{{$contact->message}}</p>
                                        </div>
                                        <div>
                                            <hr class="m-t-0">
                                        </div>
                                        <div class="card-body">
                                            <div class="b-all m-t-20 p-20">
                                                <p class="p-b-20">click here to <a href="{{route('admin.contacts')}}">Back</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection