@extends('dashboard.layouts.app')
@section('content')
<div class="container-fluid r-aside">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            </ol>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <div class="fa fa-envelope display-5 op-3 text-dark"></div>
                        <div class="align-self-center" style="margin-left: 20px;">
                            <h6 class="text-muted m-t-10 m-b-0">Total Contacts</h6>
                            <h2 class="m-t-0">    @php
                                    $contactsCount = \App\Contact::all();
                                @endphp
                                {{count($contactsCount)}}</h2></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <div class="m-r-20 align-self-center"><span class="lstick m-r-20"></span><img src="{{asset('assets/dashboard/images/icon/expense.png')}}" alt="Income" /></div>
                        <div class="align-self-center">
                            <h6 class="text-muted m-t-10 m-b-0">Total Articles</h6>
                            <h2 class="m-t-0">
                                @php
                                    $articlesCount = \App\Article::all();
                                @endphp
                                {{count($articlesCount)}}
                            </h2></div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!-- ============================================================== -->
    <!-- Projects of the month -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <div>
                            <h4 class="card-title"><span class="lstick"></span>Last 4 Articles</h4></div>

                    </div>
                    <div class="table-responsive m-t-20 no-wrap">
                        <table class="table vm no-th-brd pro-of-month">
                            <thead>
                            <tr>
                                <th>image</th>
                                <th>title</th>
                                <th>text</th>
                                <th>created at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Article::take('4')->orderBy('id','desc')->get() as $article)
                            <tr class="active">
                                <td><span class="round"><img src="{{ asset($article->image)}}" alt="user" width="50"></span></td>
                                <td>
                                    <h6>{{$article->title}}</h6></td>
                                <td>{{$article->text}}</td>
                                <td><span class="label label-info label-rounded">{{$article->created_at}}</span></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex no-block">
                        <div>
                            <h4 class="card-title"><span class="lstick"></span>Last 4 Contacts Messages</h4></div>

                    </div>
                    <div class="table-responsive m-t-20 no-wrap">
                        <table class="table vm no-th-brd pro-of-month">
                            <thead>
                            <tr>
                                <th>name</th>
                                <th>email</th>
                                <th>subject</th>
                                <th>message</th>
                                <th>created at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Contact::take('4')->orderBy('id','desc')->get() as $contact)
                                <tr class="active">
                                    <td>
                                    <h6>{{$contact->name}}</h6></td>
                                    <td>{{$contact->email}}</td>
                                    <td>{{$contact->subject}}</td>
                                    <td>{{$contact->message}}</td>
                                    <td><span class="label label-info label-rounded">{{$contact->created_at}}</span></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection