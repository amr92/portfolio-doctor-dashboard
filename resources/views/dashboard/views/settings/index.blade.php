@extends('dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Form validation</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Settings</li>
                </ol>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Settings</h4>
                        <form class="m-t-40" novalidate action="{{route('admin.settings.update')}}" method="POST" >
                            @csrf
                            <div class="form-group">
                                <label for="blog_name">facebook</label>
                                <input type="text" class="form-control" name="facebook"  value="{{$settings->facebook}}">
                            </div>

                            <div class="form-group">
                                <label for="phone_number">Phone </label>
                                <input type="text" class="form-control" name="phone"  value="{{$settings->phone}}">
                            </div>


                            <div class="form-group">
                                <label for="blog_email">Email</label>
                                <input type="text" class="form-control" name="email"  value="{{$settings->email}}">
                            </div>

                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address"  value="{{$settings->address}}">
                            </div>
                            <div class="form-group">
                                <h5>Textarea <span class="text-danger">*</span></h5>
                                <div class="controls">
                                    <textarea name="about" id="about" class="form-control" required placeholder="about">{{$settings->about}}</textarea>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <a href="{{route('dashboard')}}"  class="btn btn-inverse">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->

    </div>

@endsection