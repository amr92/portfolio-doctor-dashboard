<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'login','namespace'=>'Authentication'], function (){
    Route::get('/','LoginController@index')->name('admin.login');
    Route::post('/','LoginController@admin_login')->name('login');
});

Route::group(['prefix'=>'admin','middleware'=>'auth','namespace'=>'Admin'], function (){
Route::get('/dashboard','DashboardController@index')->name('dashboard');
Route::get('/logout', 'DashboardController@logout')->name('logout');

    Route::prefix('contacts')->group(function () {
        Route::get('/','ContactsController@index')->name('admin.contacts');
        Route::get('/{id}','ContactsController@show')->name('admin.contacts.show');
        Route::delete('/{id}','ContactsController@destroy')->name('admin.contacts.destroy');
    });

    Route::prefix('articles')->group(function (){
       Route::get('/','ArticlesController@index')->name('admin.articles');
        Route::get('/create', 'ArticlesController@create')->name('admin.articles.create');
        Route::post('/store', 'ArticlesController@store')->name('admin.articles.store');
        Route::get('/edit/{id}', 'ArticlesController@edit')->name('admin.articles.edit');
        Route::post('/update/{id}', 'ArticlesController@update')->name('admin.articles.update');
       Route::delete('/{id}','ArticlesController@destroy')->name('admin.articles.destroy');
    });

    Route::prefix('settings')->group(function (){
        //route for Settings
        Route::get('/', 'SettingsController@index')->name('admin.settings');
        Route::post('/update', 'SettingsController@update')->name('admin.settings.update');
    });

    Route::prefix('profile')->group(function (){
        //route for Settings
        Route::get('/', 'ProfilesController@index')->name('admin.profile');
        Route::post('/update', 'ProfilesController@update')->name('admin.profile.update');
    });
});



//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('admin/dashboard', function () {
//    return view('dashboard.views.dashboard.index');
//});


